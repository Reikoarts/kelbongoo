# Catalogue E-commerce Kelbongoo

## Description
Ce projet implémente une interface simple de catalogue e-commerce, avec deux vues principales :
1. **Inventaire Admin** : Permet l'édition du catalogue par un admin.
2. **Catalogue Client** : Permet aux clients d’ajouter des produits dans leur panier et de les commander.

## Techno
- Framework : NextJs
- ORM : PrismaJs

## Fonctionnalités
- Gestion de l'inventaire par l'administrateur.
- Vue client pour sélectionner et commander des produits.

## Installation
Clonez le dépôt et installez les dépendances :

```bash
git clone
cd [nom_du_projet]
npm install
```

## Setup Prismajs
Pour configurer PrismaJS, commencez par créer une base de données nommée X manuellement. Ensuite, accédez à votre fichier .env et ajoutez l'URL de connexion MySQL de votre base de données locale. Utilisez la variable d'environnement suivante pour ce faire : DATABASE_URL.

```bash
npx prisma migrate dev --name init
```

## Lancer le projet
```bash
npm run dev
```

## Navigation repo
La branche principale (main) représente la version finale du projet. Cependant, toutes les branches développées au cours du projet restent accessibles pour permettre une exploration complète du processus de développement.

## Structure du code
La structure du code peut sembler désorganisée à première vue. Mon objectif a été de regrouper toutes les fonctions et variables liées dans le même endroit afin de faciliter la compréhension et la lecture du code.

## Récapitulatif du test
Une grande partie du test a été complétée avec succès. Toutefois, il reste un détail à régler : le total du panier ne se met pas à jour automatiquement et nécessite un rafraîchissement manuel de la page pour afficher les changements.

## Capture d'écran
### Dashboard admin pour la saisie d'inventaire :
![formulaire de saisie admin](https://image.noelshack.com/fichiers/2023/47/4/1700770751-question-1-2.png)

![Dashboard admin](https://image.noelshack.com/fichiers/2023/47/4/1700770890-dashboard-admin.png)


### Catalogue client :
![Catalogue client](https://image.noelshack.com/fichiers/2023/47/4/1700772201-question-4.png)


# Remerciement
Je tiens à exprimer ma profonde gratitude à Kelbongoo pour m'avoir accordé cette fantastique opportunité de participer à ce test technique. Cette expérience a été à la fois enrichissante et stimulante, me permettant de mettre en pratique mes compétences tout en me confrontant à de nouveaux défis.

Je suis reconnaissant pour le temps et les ressources investis dans ce processus de sélection. L'expérience acquise au cours de ce test sera sans aucun doute précieuse pour mes projets futurs, et je suis enthousiaste à l'idée de potentielles collaborations avec votre équipe.

# Information complémentaire
Nathan MACAIGNE

Date du début de projet : 16/11/2023
Date de rendu de projet : 23/11/2023



