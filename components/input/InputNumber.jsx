import React from 'react'
import styles from '../../styles/Input.module.css'

const InputNumber = ({name, disable, type, setHtPrice, value, setStockCommande, setStock}) => {
  return (
    <div className='flexcol'>
      <label htmlFor='inputText' className={styles.labelText}>{name}</label>
      <input type='number' disabled={disable == "true" ? true : false} placeholder='Stock' defaultValue={type == "ttcprice" ? value : 0} 
      className={styles.inputNumber} min={0} 
      onChange={(e)=>{

        //boucle de set en fonction du type de input
        if(type === 'price'){
          setHtPrice(e.target.value)
        } else if (type === 'stock'){
          setStock(e.target.value)
        } else if (type === 'stockOrdered'){
          setStockCommande(e.target.value)
        }

      }}/>
    </div>
  )
}

export default InputNumber