import React from 'react'
import styles from '../../styles/Input.module.css'

const InputSelect = ({ name, setTva }) => {
    return (
        <div className='flexcol'>
            <label htmlFor='select' className={styles.labelText}>{name}</label>
            <select placeholder='Catégorie' className={styles.select} 
            onChange={(e)=>{setTva(e.target.value)}}>
                <option value=''>TVA</option>
                <option value='5.5'>5.5%</option>
                <option value='20'>20%</option>
            </select>
        </div>
    )
}

export default InputSelect