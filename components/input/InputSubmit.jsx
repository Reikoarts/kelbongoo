import React from 'react';
import styles from '../../styles/Input.module.css';
import Router from 'next/router';

const SubmitButton = ({ value, handleSubmit }) => {
    return (
        <input type="submit" value={value} className={styles.inputSubmit} onClick={handleSubmit} />
    );
}

export default SubmitButton;
