import React from 'react'
import styles from '../../styles/Input.module.css'

const InputText = ({name, setProduct}) => {
  return (
    <div className='flexcol'>
      <label htmlFor='inputText' className={styles.labelText}>{name}</label>
      <input type='text' placeholder='Nom du produit' className={styles.inputText} onChange={(e)=>{setProduct(e.target.value)}}/>
    </div>

  )
}

export default InputText