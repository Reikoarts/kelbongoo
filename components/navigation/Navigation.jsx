import React from 'react'
import styles from '../../styles/Navigation.module.css'
import Logo from '../../assets/img/logo.webp'
import Image from 'next/image'

const Navigation = () => {
  return (
    <section className={styles.navigation}>
        <div>
            <Image src={Logo} alt="Logo" className={styles.logo} />
            <ul className={styles.linkList}>
                <a href="/"><li>Catalogue</li></a>
                <a href="/dashboard"><li>Dashboard</li></a>
            </ul>
        </div>
    </section>
  )
}

export default Navigation