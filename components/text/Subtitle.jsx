import React from 'react'
import styles from '../../styles/Subtitle.module.css'

const Subtitle = ({content}) => {
  return (
    <h2 className={styles.subtitle}>{content}</h2>
  )
}

export default Subtitle