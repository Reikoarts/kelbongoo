import React from 'react'
import styles from '../../styles/Title.module.css'

const Title = ({content}) => {
  return (
    <h1 className={styles.title}>{content}</h1>
  )
}

export default Title