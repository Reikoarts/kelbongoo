import prisma from "lib/prisma"

export default async function handle(req, res) {

    switch (req.method) {
        case 'GET':
            try {
                const carts = await prisma.cart.findMany({
                    orderBy: [{ id: 'desc' }],
                    include: {
                        items : {
                            include: {
                                product: true
                            }
                        }
                    }
                });
                res.status(200).json(carts);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la récupération des produits' });
            }
            break;


        case 'POST':
            
            try {
                const cart = await prisma.cart.create({
                    data: {
                        checked_out: req.body.checked_out,
                    }
                });
                res.status(200).json(cart);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la création du produit' });
            }
            break;

        case 'PATCH':
            try {
                const cart = await prisma.cart.update({
                    where: {
                        id: parseInt(req.body.id)
                    },
                    data: {
                        checked_out: req.body.checked_out,
                    }
                });
                res.status(200).json(cart);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la création du produit' });
            }
            break;

        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}