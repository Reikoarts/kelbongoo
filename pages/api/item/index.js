import prisma from "lib/prisma"

export default async function handle(req, res) {

    switch (req.method) {
        case 'GET':
            try {
                const items = await prisma.item.findMany({
                    orderBy: [{ id: 'desc' }]
                });
                res.status(200).json(items);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la récupération des produits' });
            }
            break;

        case 'POST':
            try {
                const item = await prisma.item.create({
                    data: {
                        cart_id: req.body.cart_id,
                        product_id: req.body.product_id,
                        quantity: req.body.quantity
                    }
                });
                res.status(200).json(item);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la création du produit' });
            }
            break;

        case 'PATCH':
            try {
                // Recherchez d'abord l'élément
                const foundItem = await prisma.item.findFirst({
                  where: {
                    cart_id: req.body.cart_id,
                    product_id: req.body.product_id,
                  },
                });
              
                // Vérifiez si l'élément a été trouvé
                if (!foundItem) {
                  return res.status(404).json({ message: 'Item not found' });
                }
              
                // Puis mettez-le à jour
                const item = await prisma.item.update({
                  where: {
                    id: foundItem.id, // Utilisez l'ID unique de l'élément trouvé
                  },
                  data: {
                    quantity: req.body.quantity,
                  },
                });
              
                res.status(200).json(item);
              } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la mise à jour du produit' });
              }
              break;

              case 'DELETE':
                try {
                    // Recherchez d'abord l'élément
                    const foundItem = await prisma.item.findFirst({
                      where: {
                        cart_id: req.body.cart_id,
                        product_id: req.body.product_id,
                      },
                    });
                  
                    // Vérifiez si l'élément a été trouvé
                    if (!foundItem) {
                      return res.status(404).json({ message: 'Item not found' });
                    }
                  
                    // Puis mettez-le à jour
                    const item = await prisma.item.delete({
                      where: {
                        id: foundItem.id, // Utilisez l'ID unique de l'élément trouvé
                      },
                    });
                  
                    res.status(200).json(item);
                  } catch (error) {
                    res.status(500).json({ message: 'Erreur lors de la mise à jour du produit' });
                  }
                    break;
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}