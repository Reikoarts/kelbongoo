import prisma from "lib/prisma"

export default async function handle(req, res) {
            try {
                // Récupérer le produit existant
                const existingProduct = await prisma.product.findUnique({
                    where: {
                        id: req.body.product_id,
                    }
                });

                // Vérifier si le produit existe
                if (!existingProduct) {
                    return res.status(404).json({ message: 'Produit non trouvé' });
                }

                const item = await prisma.product.update({
                    where: {
                        id: req.body.product_id,
                    },
                    data: {
                        stock_order: existingProduct.stock_order + req.body.quantity
                    }
                });

                res.status(200).json(item);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la mise à jour du produit' });
            }
}
