import prisma from "lib/prisma"

export default async function handle(req, res) {
    const productName = req.body.name
    const productHTPrice = req.body.price_ht
    const tva = req.body.tva
    const orderedStock = req.body.stock_order
    const stock = req.body.stock
    const productId = req.body.id


    switch (req.method) {
        case 'GET':
            try {
                const products = await prisma.product.findMany({
                    orderBy: [{ id: 'desc' }]
                });
                res.status(200).json(products);
            } catch (error) {
                res.status(500).json({ message: 'Erreur lors de la récupération des produits' });
            }
            break;


        case 'POST':
            
            const productCreate = await prisma.product.create({
                data: {
                    name: productName,
                    price_ht: productHTPrice,
                    tva: tva,
                    stock_order: orderedStock,
                    stock: stock
                }
            })
            
            res.status(200).json(productCreate)

            break;


        case 'DELETE':
            const productDelete = await prisma.product.delete({
                where: {
                    id: productId
                }
            })
            res.status(200).json(productDelete)

            break;

        case 'PATCH':
            const productUpdate = await prisma.product.update({
                where: {
                    id: productId
                },
                data: {
                    name: productName,
                    price_ht: productHTPrice,
                    tva: tva,
                    stock_order: orderedStock,
                    stock: stock
                }
            })
            res.status(200).json(productUpdate)

            break;

        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}