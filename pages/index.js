import Navigation from '@/components/navigation/Navigation'
import Title from '@/components/text/Title'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import { getTtcPrice } from '@/utils/calc'
import Router from 'next/router'

export default function Home({ initialCart, product }) {
  const [cart, setCart] = useState(initialCart); //copie de initialCart


  useEffect(() => {
    if (cart[0] && cart[0].checked_out) { //si le panier est check out requete api pour créer un nouveau sinon garde le panier en cours --> TEST5
      const getNewCart = async () => {
        const response = await fetch('http://localhost:3000/api/cart', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ checked_out: false }),
        });
        const newCart = await response.json();
        setCart([newCart]); //set le nouveau panier dans le cart
        Router.reload();
      };
      getNewCart();
    }

  }, [cart]);

  const [total, setTotal] = useState(0)
  const [numberOfProduct, setNumberOfProduct] = useState(0)


  // État pour la liste combinée
  const [combinedList, setCombinedList] = useState([]);


  useEffect(() => {
    // Création de la liste combinée
    const updatedList = product.map(prod => {

      //flatmap permet de fusionner les tableaux en un seul tableau
      const productInCart = cart.flatMap(c => c.items).find(item => item.product.id === prod.id);

      // Utiliser la quantité du panier si le produit sinon 0
      return {
        ...prod,
        quantity: productInCart ? productInCart.quantity : 0
      };
    });

    setCombinedList(updatedList);
    
  }, [cart, product]);


  // Calcul du prix TTC a chaque changement de cart -> item -> quantity
  useEffect(() => {
    calcTotal()
  }, [cart, cart[0].items], [cart, cart[0].items])

  const calcTotal = () => {
    let tempTotal = 0
    if (cart) {
      cart.map((item) => (
        item.items.map((subItem) => {
          let calc = (subItem.product.price_ht * (1 + subItem.product.tva / 100) ) * subItem.quantity   //Calcul du prix TTC
          tempTotal = tempTotal + calc
        })
      ))

      setTotal(Math.round(tempTotal * 100) / 100)  //state du prix total

      for (let i = 0; i < cart.length; i++) {
        if (cart[i].checked_out == false && cart[i].items.length > 0) {
          setNumberOfProduct(cart[i].items.length * cart[i].items[i].quantity)  //calcul du nombre de produit + ajout quantity
        }
      }

    }
  }


  //fonction pour passer le checked_out a true
    const setCheckedOut = async () => {
      const response = await fetch('/api/cart', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id : cart[0].id,
          checked_out: true 
        }),
      })
      //recuperer la réponse
      const data = await response.json()
    }

  // Fonction pour générer les options en fonction du stock value stock = stock restant
  function generateQuantityOptions(stock) {
    let options = [];
    for (let i = 0; i < stock; i++) {
      options.push(<option key={i} value={i}>{i}</option>);
    }
    return options;
  }


  const handleQuantityChange = async (product, newQuantity, id) => {
    if (product.quantity === 0 && newQuantity > 0) {
      const response = await fetch('/api/item', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cart_id: parseInt(cart[0].id),
          product_id: parseInt(product.id),
          quantity: parseInt(newQuantity)
        }),
      })

      //recuperer la réponse
      const data = await response.json()
      Router.reload()
      
    } else if (product.quantity >= 1  && newQuantity != product.quantity && newQuantity != 0) {
      const response = await fetch('/api/item', {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cart_id :parseInt(cart[0].id),
          product_id: parseInt(product.id),
          quantity: parseInt(newQuantity)
        }),
      })
      //recuperer la réponse
      const data = await response.json()
      Router.reload()


    } else if (product.quantity >= 1 && newQuantity == 0) {
      const response = await fetch('/api/item', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          cart_id :parseInt(cart[0].id),
          product_id: parseInt(product.id),
        }),
      })
      //recuperer la réponse
      const data = await response.json()
      Router.reload()


    }

  }

  // Fonction pour préparer les données de commande
  const prepareOrderData = () => {
    const orderItems = cart.flatMap(carts =>  //Créer un tableau avec les données de la commande
      carts.items.map(item => ({
        product_id: item.product.id,
        quantity: item.quantity,
      }))
    );
  
    return orderItems;
  };



  // Fonction pour passer la commande via API
  const handleCheckout = async () => {
    const orderData = prepareOrderData();

    for(let i = 0; i < orderData.length; i++){ //Pour chaque produit dans le panier on fait une requete API pour update le stock
      const response = await fetch('/api/order', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(orderData[i]),
      });
    }

    setCheckedOut() //update le checked_out a true
  };
  
  




  return (
    <>
      <Head>
        <title>Kelbongoo</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <div>
        <Navigation />
        <section className='widthSite'>
          <Title content="Sélectionnez vos produits" />
          <section className='test'>
            <div style={{ width: "100%" }}>
              <div className="table-container">
                <table>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th className='hideInmobil'>Prix Hors taxe</th>
                      <th className='hideInmobil'>TVA</th>
                      <th>Prix TTC</th>
                      <th>Quantité</th>
                    </tr>
                  </thead>
                  <tbody>
                    {combinedList.map((item, index) => (
                      <tr key={item.id + index}>
                        <td>{item.name}</td>
                        <td className='hideInmobil'>{item.price_ht}</td>
                        <td className='hideInmobil'>{item.tva}%</td>
                        <td>{Math.round(item.price_ht * (1 + item.tva / 100) * 100) / 100}</td> {/* affichage de l'object produit */}
                        <td className='flexrow' style={{ gap: 20 }}>
                          <select onChange={(e) => {
                            handleQuantityChange(item, e.target.value, item.id)
                          }} defaultValue={item.quantity}>
                            {generateQuantityOptions(item.stock - item.stock_order +1)}
                          </select>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>

            <div className='infoClient'>
              <div className="container">
                <div className="header">
                  <p>{total} € pour {numberOfProduct} produits</p> {/* affichage du prix et nombre de produit */}
                </div>
                <div className="section">
                  <div className="section-title">Information client</div>
                  <div className="row">
                    <span className='spanTitle'>Nom</span>
                    <span className='spanTitle'>Prénom</span>
                  </div>
                  <div className="row">
                    <span>Macaigne</span>
                    <span>Nathan</span>
                  </div>
                </div>
                <div className="section">
                  <div className="section-title">Information adresse</div>
                  <div className="row">
                    <span className='spanTitle'>Rue</span>
                    <span className='spanTitle'>Numéro</span>
                  </div>
                  <div className="row">
                    <span>Lorem Ipsum</span>
                    <span>18</span>
                  </div>
                </div>
                <div className="section">
                  <div className="section-title">Information facturation</div>
                  <div className="row">
                    <span className='spanTitle'>Nom</span>
                    <span className='spanTitle'>Prénom</span>
                  </div>
                  <div className="row">
                    <span>Macaigne</span>
                    <span>Nathan</span>
                  </div>
                  <div className="row">
                    <span className='spanTitle'>Rue</span>
                    <span className='spanTitle'>Numéro</span>
                  </div>
                  <div className="row">
                    <span>Lorem Ipsum</span>
                    <span>18</span>
                  </div>
                </div>
                <div className="button" onClick={()=>{
                  handleCheckout()
                  setInterval(() => {
                    Router.reload()
                  }, 1000); //reload la page pour afficher le nouveau panier
                  }} >
                  Commander
                </div>
              </div>

            </div>


          </section>
        </section>
      </div>
    </>
  )
}


export async function getServerSideProps() {
  // Récupération des données depuis l'API
  const response = await fetch('http://localhost:3000/api/cart');
  const carts = await response.json();

  // Sélectionner le dernier panier non "checked_out"
  const activeCart = carts.find(cart => !cart.checked_out) || carts[0];

  // Récupération des données depuis l'API
  const productsResponse = await fetch('http://localhost:3000/api/product');
  const products = await productsResponse.json();

  // Renvoi des données sous forme de props
  return {
    props: {
      initialCart: [activeCart],
      product: products
    },
  };
}