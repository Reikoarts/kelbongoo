export const getTtcPrice = (htProductPrice, tvaProduct, setter) => {
    setter(Math.round(htProductPrice * (1 + tvaProduct / 100) * 100) / 100)
}
