export const getProducts = async () => {
    try {
        const products = await prisma.product.findMany({
            orderBy: [{ id: 'desc' }]
        });
        res.status(200).json(products);
    } catch (error) {
        res.status(500).json({ message: 'Erreur lors de la récupération des produits' });
    }

}


export const createProduct = async (data) => {
    const product = await prisma.product.create({
        data
    });
    return product;
}